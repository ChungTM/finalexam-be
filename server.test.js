const getShipment = require('./server.js').getShipment;
const getQuote = require('./server.js').getQuote;
const deleteShipment = require('./server.js').deleteShipment;
const validateRefId = require('./server').validateRefId;
const connectDB = require('./server').connectDB;
const url = 'mongodb://localhost:27017/';

const quote = {
    weight: 100,
    fromCountry: 'FR',
    toCountry: 'FR',
};
const wrongQuote = {
    height: 100,
};

describe('#validate RefId to be False', () => {
    test('Add not a number or number smaller than 999999999 return false', () => {
        expect(validateRefId(12121)).toEqual(false);
        expect(validateRefId(NaN)).toEqual(false);
        expect(validateRefId(-1111)).toEqual(false);
        expect(validateRefId(null)).toEqual(false);
        expect(validateRefId({})).toEqual(false);
        expect(validateRefId('string')).toEqual(false);
        expect(validateRefId([1, 2, 3])).toEqual(false);
        expect(validateRefId(0)).toEqual(false);
        expect(validateRefId(10000000000)).toEqual(false);
        expect(validateRefId('10000000000')).toEqual(false);
    });
});

describe('#Validate RefId to be true', () => {
    test('Add number greater than 999999999 and smaller 100000000000', () => {
        expect(validateRefId(1000000000)).toEqual(true);
        expect(validateRefId(9654721588)).toEqual(true);
    });
});



