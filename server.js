/* eslint-disable no-shadow */

const express = require('express');

const app = express();
const port = 3000;
const {MongoClient} = require('mongodb');

const url = 'mongodb://localhost:27017/';
const dbCollectionRate = db => db.db('shipmentDB').collection('rateData');
const dbCollectionQuote = db => db.db('shipmentDB').collection('quote');
const dbCollectionShipment = db => db.db('shipmentDB').collection('shipment');
const getQuote = query => new Promise((resolve, reject) => {
    connectDB(url)
        .then((db) => {
            const collectionRate = dbCollectionRate(db);
            return collectionRate.findOne(query);
        })
        .then(result => resolve(result))
        .catch(err => reject(err));
});
const handleError = err => new Error(err);
const validateRefId = refId => !!(refId && refId > 999999999 && refId < 10000000000);

// Set app use respone JSON
app.use(express.json());

// Set up CORS
app.use((request, response, next) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

    next();
});

// App listen on port 3000
app.listen(port, (err) => {
    if (err) throw err;
    console.log(`App listen on port ${port}`);
});

const connectDB = url => {
    return new Promise((resolve, reject) => {
        MongoClient.connect(url)
            .then(db => resolve(db))
            .catch(err => reject(err))
    })
};


app.post('/get-quote', (req, res) => {
    const {quoteData} = req.body;
    const query = {
        weight: {$gte: quoteData.weight},
        fromCountry: quoteData.fromCountry.toUpperCase(),
        toCountry: quoteData.toCountry.toUpperCase(),
    };
    getQuote(query).then(result => res.send(result)).catch(err => handleError(err));
});

// API Method POST: http://localhost:3000/create-shipment
app.post('/create-shipment', (req, response) => {
    const {data} = req.body;
    const {quotePrice} = data;
    const {shipmentData} = data;

    connectDB(url)
        .then(db => {
            const collectionShipment = dbCollectionShipment(db);
            const collectionQuote = dbCollectionQuote(db);
            collectionQuote.insertOne({quotePrice})
                .then(res => {
                    const ref = Math.floor(Math.random() * 10000000000) + 1;
                    const quoteId = res.insertedId;
                    const data = {quoteId, ...shipmentData, ref};
                    collectionShipment.insertOne(data)
                        .then(res => {
                            const result = {
                                ref: res.ops[0].ref,
                                created_at: res.insertedId.getTimestamp(),
                                cost: quotePrice,
                            };
                            response.send(result);
                            db.close();
                        })
                })

        })
        .catch(err => console.log(err));
});

const getShipment = refId => new Promise((resolve, reject) => {
    if (validateRefId(refId)) {
        connectDB(url)
            .then((db) => {
                const collectionShipment = dbCollectionShipment(db);
                collectionShipment.findOne({ref: refId})
                    .then((res) => {
                        if (res === null) {
                            resolve(null);
                        } else {
                            resolve(res);
                        }
                    });
                db.close();
            })
            .catch(err => handleError(err));
    } else {
        reject(new Error('Null'));
    }
});

// API Method GET http://localhost:3000/getShipment/:refId
app.get('/getShipment/:refId', (req, response) => {
    const refId = +req.params.refId;
    getShipment(refId).then(result => response.send(result)).catch(err => response.send(err));
});

const deleteShipment = refId => new Promise((resolve, reject) => {
    if (validateRefId(refId)) {
        connectDB(url)
            .then((db) => {
                const collectionShipment = dbCollectionShipment(db);
                collectionShipment.deleteOne({ref: refId})
                    .then((result) => {
                        if (result.deletedCount === 1) {
                            resolve(
                                {
                                    status: 'OK',
                                    message: 'Shipment has been deleted',
                                },
                            );
                        } else {
                            resolve({
                                status: 'NOK',
                                message: 'Shipment not found',
                            });
                        }
                    });
                db.close();
            })
            .catch(err => handleError(err));
    } else {
        reject(new Error('Shipment not found'));
    }
});
// API Method DELETE http://localhost:3000/deleteShipment/:refId

app.delete('/deleteShipment/:refId', (req, res) => {
    const refId = +req.params.refId;
    deleteShipment(refId).then(result => res.send(result)).catch(err => res.send(err));
});
module.exports = {getQuote, getShipment, deleteShipment, validateRefId, connectDB};
